# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0-alpha.9](https://gitlab.com/koober-sas/analytics/compare/v1.0.0-alpha.8...v1.0.0-alpha.9) (2021-09-01)


### Bug Fixes

* correct all lint errors ([f3d0d52](https://gitlab.com/koober-sas/analytics/commit/f3d0d523c8997a76c3db077bbb8b462984370ca7))
* correct typing ([4d9eb2a](https://gitlab.com/koober-sas/analytics/commit/4d9eb2ad32a9463ba51105f27c330f5ac407170f))
* **deps:** correct eslint deps ([47cff98](https://gitlab.com/koober-sas/analytics/commit/47cff98b26c6fe90a47b1f38eb8f57d6780be471))





# [1.0.0-alpha.8](https://gitlab.com/koober-sas/analytics/compare/v1.0.0-alpha.7...v1.0.0-alpha.8) (2021-02-22)


### Bug Fixes

* **ts:** improve identify event typing ([2eb67fb](https://gitlab.com/koober-sas/analytics/commit/2eb67fb71ec1cbf4f3715db1551acf564352bc77))





# [1.0.0-alpha.7](https://gitlab.com/koober-sas/analytics/compare/v1.0.0-alpha.6...v1.0.0-alpha.7) (2021-02-22)


### Bug Fixes

* **ts:** make properties readonly ([33c152d](https://gitlab.com/koober-sas/analytics/commit/33c152dd62fc71927d15dae0f33abd8c06a9f5ab))





# [1.0.0-alpha.6](https://gitlab.com/koober-sas/analytics/compare/v1.0.0-alpha.5...v1.0.0-alpha.6) (2021-02-17)


### Bug Fixes

* **git:** correct git hooks ([eb017d6](https://gitlab.com/koober-sas/analytics/commit/eb017d6bf181a5386ac3929ab217de1f5d1280e6))
* **mrm:** change archetype to workspace ([dde742e](https://gitlab.com/koober-sas/analytics/commit/dde742e14d90a8fcfc6bf99916bcd4ca2e4624af))





# [1.0.0-alpha.5](https://gitlab.com/koober-sas/analytics/compare/v1.0.0-alpha.4...v1.0.0-alpha.5) (2020-09-16)


### Bug Fixes

* **npm:** add prerelease task ([67ea086](https://gitlab.com/koober-sas/analytics/commit/67ea0863d1d16846c797683b7b17deefeaabc45e))





# [1.0.0-alpha.4](https://gitlab.com/koober-sas/analytics/compare/v1.0.0-alpha.3...v1.0.0-alpha.4) (2020-09-16)


### Features

* **event:** add occurred property ([47294f9](https://gitlab.com/koober-sas/analytics/commit/47294f9eb62cd452ca9e375856ae4da51c1971f5))





# [1.0.0-alpha.3](https://gitlab.com/koober-sas/analytics/compare/v1.0.0-alpha.2...v1.0.0-alpha.3) (2020-09-15)

**Note:** Version bump only for package analytics





# 1.0.0-alpha.2 (2020-09-08)


### Bug Fixes

* **npm:** correct release command ([58adc50](https://gitlab.com/koober-sas/analytics/commit/58adc50c6dc3494821c8d1ff6431fd22ae96a482))


### Features

* **analytics:** add workspace structure ([12b977b](https://gitlab.com/koober-sas/analytics/commit/12b977b136c7ccd5db158520a6b32594536a161e))
* **analytics:** import analytics from rn app ([ed415df](https://gitlab.com/koober-sas/analytics/commit/ed415df558b7a2e2efcbf949666bbef16bf6fd0b))
* **config:** init project ([ee7e5cc](https://gitlab.com/koober-sas/analytics/commit/ee7e5cc97099be1ad9b9253047fe6b36e8ed88f7))
