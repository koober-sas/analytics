import { DataObject, JSONValue as StdJSONValue } from '@koober/std';
import { Identity } from './identity';

type EmptyObject = Record<string, never>;
type AnyObject = Record<string, unknown>;
type Identifier = number | string;

export type BuildId = Identifier;
export type Version = number | string;
export type JSONValue = StdJSONValue;

export type EventContext = AnyObject;

type AnyEventProperties = {
  [property: string]: any;
};

export type Event =
  | Event.Identify<Identity, AnyObject>
  | Event.Screen<AnyObject>
  | Event.Page<AnyObject>
  | Event.Track<string, AnyObject>;

/**
 * Common events factories
 */
export namespace Event {
  type ISODateString = string;

  type OptionalPropertyNames = 'context';
  type Parameters<T extends { context: unknown }> = Omit<T, DataObject.type | OptionalPropertyNames> &
    Partial<Pick<T, OptionalPropertyNames>>;

  type BaseEvent<EventName extends string, EventProperties extends { properties: AnyObject }> = DataObject<
    {
      _type: EventName;
      /**
       *
       */
      // properties: EmptyObject;
      /**
       * The context of the event : ip address, etc.
       */
      context: EventContext;
      /**
       * An ISO date string representing when the event was occurred.
       */
      occurred?: ISODateString;
    } & EventProperties
  >;

  export interface Identify<I extends Identity, Properties extends AnyObject>
    extends BaseEvent<
      'Event/Identify',
      {
        /**
         * The identity of the user who triggered the event
         */
        identity: I;
        properties: Readonly<Properties>;
      }
    > {}
  /**
   * Returns a new event object representing a user identification
   *
   * @param identity - the user identity
   * @returns the new event
   */
  export const Identify = DataObject.MakeGeneric(
    'Event/Identify',
    (create) =>
      <I extends Identity>(identity: I): Event.Identify<I, EmptyObject> =>
        create({
          identity,
          properties: {},
          context: {}, // TODO refactor identify signature
        })
  );

  export interface Page<Properties extends AnyObject>
    extends BaseEvent<
      'Event/Page',
      {
        properties: Readonly<Properties>;
        /**
         * The page url where the event occurred
         */
        url: string;
      }
    > {}
  /**
   * Returns a new event object representing a page view action (on web)
   *
   * @param payload - the event payload
   * @returns the new event
   */
  export const Page = DataObject.MakeGeneric(
    'Event/Page',
    (create) =>
      <Properties extends AnyEventProperties>({
        url,
        properties,
        context = {},
        occurred,
      }: Parameters<Page<Properties>>): Event.Page<Properties> =>
        create({
          url,
          properties,
          context,
          occurred,
        })
  );

  export interface Screen<Properties extends AnyObject>
    extends BaseEvent<
      'Event/Screen',
      {
        properties: Readonly<Properties>;
        /**
         * The screen name
         */
        name: string;
      }
    > {}
  /**
   * Returns a new event object representing a screen view (on mobile)
   *
   * @param payload - the event payload
   * @returns the new event
   */
  export const Screen = DataObject.MakeGeneric(
    'Event/Screen',
    (create) =>
      <Properties extends AnyObject>({
        name,
        properties,
        context = {},
        occurred,
      }: Parameters<Screen<Properties>>): Event.Screen<Properties> =>
        create({
          name,
          properties,
          context,
          occurred,
        })
  );

  export type Track<EventName, Properties extends AnyObject> = BaseEvent<
    'Event/Track',
    {
      /**
       * The event name
       */
      event: EventName;
      properties: Readonly<Properties>;
    }
  >;
  /**
   * Returns a new event object for a custom action
   *
   * @param payload - the event payload
   * @returns the new event
   */
  export const Track = DataObject.MakeGeneric(
    'Event/Track',
    (create) =>
      <EventName extends string, Properties extends AnyEventProperties>({
        event,
        properties,
        context = {},
        occurred,
      }: Parameters<Track<EventName, Properties>>): Event.Track<EventName, Properties> =>
        create({
          event,
          properties,
          context,
          occurred,
        })
  );
}
