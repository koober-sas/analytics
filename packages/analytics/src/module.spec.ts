import { Identity } from './identity';
import { AnalyticsModule } from './module';
import { Event } from './event';

describe('AnalyticsModule', () => {
  const createModule = AnalyticsModule;
  const getHandlers = (_: AnalyticsModule) => _.getHandlers();
  const anyEvent = {
    identity: Identity.Anonymous('anonymousId'),
    event: Event.Track({
      event: 'Test Event',
      properties: {},
    }),
  };

  describe('track()', () => {
    test('should call all handlers', () => {
      const handler = jest.fn();
      const sys = createModule({ initialState: { handlers: [handler] } });
      sys.track(anyEvent.identity, anyEvent.event);

      expect(handler).toHaveBeenCalledWith(anyEvent);
    });
  });

  describe('handle()', () => {
    test('should call all handlers', () => {
      const handler = jest.fn();
      const sys = createModule({ initialState: { handlers: [handler] } });
      sys.handle(anyEvent);

      expect(handler).toHaveBeenCalledWith(anyEvent);
    });

    test('should call all even if one throws an error', () => {
      const thrownError = new Error('MockError');
      const onError = jest.fn();
      const handlerThrow = jest.fn(() => {
        throw thrownError;
      });
      const handler = jest.fn();
      const sys = createModule({
        initialState: { handlers: [handlerThrow, handler] },
        onError,
      });
      sys.handle(anyEvent);

      expect(handler).toHaveBeenCalledWith(anyEvent);
      expect(handlerThrow).toHaveBeenCalledWith(anyEvent);
      expect(onError).toHaveBeenCalledWith(thrownError);
    });
  });

  describe('setHandlers()', () => {
    test('should replace handlers', () => {
      const sys = createModule({ initialState: { handlers: [jest.fn()] } });
      const handlers = [jest.fn(), jest.fn()];
      sys.setHandlers(handlers);
      expect(getHandlers(sys)).toEqual(handlers);
    });
  });

  describe('addHandler()', () => {
    test('should add at the end a new handler', () => {
      const sys = createModule();
      const newHandler = jest.fn();
      sys.addHandler(newHandler);
      expect(getHandlers(sys)).toEqual([newHandler]);
    });

    test('should append handler to handler list', () => {
      const defaultHandler = jest.fn();
      const sys = createModule({
        initialState: {
          handlers: [defaultHandler],
        },
      });

      const newHandler = jest.fn();
      sys.addHandler(newHandler);
      expect(getHandlers(sys)).toEqual([defaultHandler, newHandler]);
    });
  });

  describe('removeHandler()', () => {
    test('should remove handler from state', () => {
      const defaultHandler = jest.fn();
      const sys = createModule({
        initialState: {
          handlers: [defaultHandler],
        },
      });

      sys.removeHandler(defaultHandler);
      expect(getHandlers(sys)).toEqual([]);
    });
  });
});
