import { DataObject } from '@koober/std';
import { Event } from './event';

describe('Event', () => {
  const anyISOString = new Date().toISOString();
  describe('Page()', () => {
    test('should create an event with default properties', () => {
      expect(
        Event.Page({
          url: 'foo',
          properties: { bar: 'baz' },
          context: { contextKey: 'contextValue' },
          occurred: anyISOString,
        })
      ).toEqual({
        [DataObject.type]: 'Event/Page',
        url: 'foo',
        properties: { bar: 'baz' },
        context: {
          contextKey: 'contextValue',
        },
        occurred: anyISOString,
      });
    });
  });
  describe('Screen()', () => {
    test('should create an event with default properties', () => {
      expect(
        Event.Screen({
          name: 'foo',
          properties: { bar: 'baz' },
          context: { contextKey: 'contextValue' },
          occurred: anyISOString,
        })
      ).toEqual({
        [DataObject.type]: 'Event/Screen',
        name: 'foo',
        properties: { bar: 'baz' },
        context: {
          contextKey: 'contextValue',
        },
        occurred: anyISOString,
      });
    });
  });
  describe('track()', () => {
    test('should create an event with default properties', () => {
      expect(
        Event.Track({
          event: 'foo',
          properties: {},
          context: { contextKey: 'contextValue' },
          occurred: anyISOString,
        })
      ).toEqual({
        [DataObject.type]: 'Event/Track',
        event: 'foo',
        properties: {},
        context: {
          contextKey: 'contextValue',
        },
        occurred: anyISOString,
      });
    });
  });
});
