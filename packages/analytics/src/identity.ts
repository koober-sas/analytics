import { DataObject } from '@koober/std';

export type UserId = string | number;

export type Identity = Identity.Anonymous | Identity.User;

export namespace Identity {
  /**
   * @see https://segment.com/docs/spec/identify/#traits
   */
  export type Traits = Readonly<{
    address?: Readonly<{
      city?: string;
      country?: string;
      postalCode?: string;
      state?: string;
      street?: string;
    }>;
    age?: number;
    avatar?: string;
    birthday?: Date;
    company?: Readonly<{
      employeeCount?: number;
      id?: UserId;
      industry?: string;
      name?: string;
      plan?: string;
    }>;
    createAt?: Date;
    description?: string;
    email?: string;
    firstName?: string;
    gender?: string;
    id?: string;
    lastName?: string;
    name?: string;
    phone?: string;
    userSegment?: string;
    title?: string;
    username?: string;
    website?: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: any;
  }>;

  export interface Anonymous
    extends DataObject<{
      _type: 'Identity/Anonymous';
      anonymousId: UserId;
      traits: Traits;
    }> {}

  /**
   * The identity of an anonymous user
   *
   * @param anonymousId - the user id, leave it blank to generate automatically
   * @param traits - properties that will be always sent
   */
  export const Anonymous = DataObject.MakeGeneric(
    'Identity/Anonymous',
    (create) =>
      (anonymousId: UserId, traits: Traits = {}): Anonymous =>
        create({
          anonymousId,
          traits,
        })
  );

  // alias
  export const isAnonymous = Anonymous.hasInstance;

  export interface User
    extends DataObject<{
      _type: 'Identity/User';
      traits: Traits;
      userId: UserId;
    }> {}

  /**
   * The identity of a connected user
   *
   * @param userId - the user id
   * @param traits - properties that will be always sent
   */
  export const User = DataObject.MakeGeneric(
    'Identity/User',
    (create) =>
      (userId: UserId, traits: Traits = {}): User =>
        create({
          userId,
          traits,
        })
  );

  // alias
  export const isUser = User.hasInstance;

  /**
   * Return the identity identifier
   *
   * @param identity - the identity
   * @returns the user id or anonymous id
   */
  export function getId(identity: Identity): UserId {
    return isAnonymous(identity) ? identity.anonymousId : identity.userId;
  }
}
