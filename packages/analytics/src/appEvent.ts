import { Event, BuildId, Version } from './event';

/**
 * Mobile app event factories
 */
export namespace AppEvent {
  export const Identify = Event.Identify;

  export const Screen = Event.Screen;

  export const Track = Event.Track;

  export const installed = <P extends { build: BuildId; version: Version }>(properties: P) =>
    Event.Track({
      event: 'Application Installed',
      properties,
    });

  export const backgrounded = () =>
    Event.Track({
      event: 'Application Backgrounded',
      properties: {},
    });

  export const updated = <
    P extends {
      build: BuildId;
      previous_build: BuildId;
      previous_version: Version;
      version: Version;
    }
  >(
    properties: P
  ) =>
    Event.Track({
      event: 'Application Updated',
      properties,
    });

  export const uninstalled = () =>
    Event.Track({
      event: 'Application Uninstalled',
      properties: {},
    });

  export const crashed = () =>
    Event.Track({
      event: 'Application Crashed',
      properties: {},
    });
}
