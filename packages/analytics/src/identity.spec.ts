import { Identity } from './identity';

describe('Identity', () => {
  describe('.getId()', () => {
    test('should return anonymousId for anonymous', () => {
      expect(Identity.getId(Identity.Anonymous('anyId'))).toEqual('anyId');
    });

    test('should return userId for non anonymous', () => {
      expect(Identity.getId(Identity.User('anyId'))).toEqual('anyId');
    });
  });
  describe('.isAnonymous()', () => {
    test('should return true only for Anonymous', () => {
      expect(Identity.isAnonymous(Identity.Anonymous('anyId'))).toBe(true);
      expect(Identity.isAnonymous(Identity.User('anyId'))).toBe(false);
    });
  });
  describe('.isUser()', () => {
    test('should return true only for Anonymous', () => {
      expect(Identity.isUser(Identity.Anonymous('anyId'))).toBe(false);
      expect(Identity.isUser(Identity.User('anyId'))).toBe(true);
    });
  });
});
