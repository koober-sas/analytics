import { AnalyticsModule, EventRecordHandler, EventRecord } from './module';

const analyticsModule = AnalyticsModule();

export const track = analyticsModule.track.bind(analyticsModule);

export const addHandler = analyticsModule.addHandler.bind(analyticsModule);

export const removeHandler = analyticsModule.removeHandler.bind(analyticsModule);

export const setHandlers = analyticsModule.setHandlers.bind(analyticsModule);

export type { EventRecordHandler, EventRecord };

export * from './identity';
export * from './event';
export * from './appEvent';
