/* eslint-disable import/export */
import { Event } from './event';
import { Identity } from './identity';

export interface EventRecord
  extends Readonly<{
    event: Event;
    identity: Identity;
  }> {}

export type EventRecordHandler = (event: EventRecord) => void;

export interface AnalyticsModule {
  /**
   * Track an `event`
   *
   * @example
   *
   * track(Identity.User('my_id'), Event.Track({ event: 'Foo Done', properties: {} }))
   *
   * @param identity - the identity of the sender
   * @param event - the event to send
   */
  track(identity: Identity, event: Event): void;

  /**
   * Send a record to all handlers
   *
   * @param eventRecord - the log record to handle
   */
  handle(eventRecord: EventRecord): void;

  /**
   * Return the current handlers in the state
   *
   * @param handlers - the new handlers
   */
  getHandlers(): Array<EventRecordHandler>;

  /**
   * Replace all system handlers by `handlers`
   *
   * @param handlers - the new handlers
   */
  setHandlers(handlers: Array<EventRecordHandler>): void;

  /**
   * Add a new handler to the system handlers
   *
   * @param handler - the new handler
   */
  addHandler(handler: EventRecordHandler): void;

  /**
   * Remove a handler from the system handlers
   *
   * @param handler - the handler to remove
   */
  removeHandler(handler: EventRecordHandler): void;
}

/**
 * Create a new system from parameters
 *
 * @param parameters - the module configuration
 * @returns a new AnalyticsModule instance
 */
export function AnalyticsModule(parameters: AnalyticsModule.Parameters = {}): AnalyticsModule {
  const microtask =
    typeof queueMicrotask !== 'undefined'
      ? queueMicrotask
      : (fn: () => void) => {
          // eslint-disable-next-line promise/catch-or-return,@typescript-eslint/no-floating-promises
          Promise.resolve().then(fn);
        };
  let state: AnalyticsModule.State = parameters.initialState ?? AnalyticsModule.initialState;

  function setState(updater: (currentState: AnalyticsModule.State) => AnalyticsModule.State): void {
    state = updater(state);
  }

  function callHandler(handler: EventRecordHandler, eventRecord: EventRecord): void {
    try {
      handler(eventRecord);
    } catch (error_: unknown) {
      handleError(error_);
    }
  }

  function track(identity: Identity, event: Event): void {
    handle({ identity, event });
  }

  function handle(eventRecord: EventRecord): void {
    const handlers = state.handlers;
    const length = handlers.length;
    for (let index = 0; index < length; index += 1) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      callHandler(handlers[index]!, eventRecord);
    }
  }

  function handleError(error: unknown) {
    if (parameters.onError != null) {
      parameters.onError(error);
    } else {
      microtask(() => {
        throw error;
      });
    }
  }

  function getHandlers(): Array<EventRecordHandler> {
    return [...state.handlers];
  }

  function setHandlers(handlers: Array<EventRecordHandler>): void {
    setState((currentState) => ({ ...currentState, handlers: [...handlers] }));
  }

  function addHandler(handler: EventRecordHandler): void {
    setState((currentState) => ({
      ...currentState,
      handlers: [...currentState.handlers, handler],
    }));
  }

  function removeHandler(handler: EventRecordHandler): void {
    setState((currentState) => ({
      ...currentState,
      handlers: currentState.handlers.filter((_) => _ !== handler),
    }));
  }

  return {
    track,
    handle,
    getHandlers,
    addHandler,
    removeHandler,
    setHandlers,
  };
}
export namespace AnalyticsModule {
  export type State = Readonly<{
    handlers: ReadonlyArray<EventRecordHandler>;
  }>;

  export type Parameters = {
    initialState?: State;

    /**
     * When an unexpected error is caught
     */
    onError?: (error: unknown) => void;
  };

  export const initialState: State = {
    handlers: [],
  };
}
