import { AppEvent, track, Identity } from '@koober/analytics';

export function main(): void {
  track(
    Identity.User('myId'),
    AppEvent.Track({
      event: 'Example',
      properties: {
        myProperty: true,
      },
    })
  );
}
